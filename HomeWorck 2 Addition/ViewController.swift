//
//  ViewController.swift
//  HomeWorck 2 Addition
//
//  Created by Volodymyr on 4/15/19.
//  Copyright © 2019 Volodymyr. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        higestNumber()
        squarAndCube()
        recount(13)
        totalNumberOfDivisors()
        perfectNunber()
        interestOnDeposit(2019)
        scholarship(scholatshipInMounth: 700, costsNumb: 1000, inflationNumb: 3)
        zadachyaIII(accumulation: 3000, scholarshipInMounht: 750)
        inversion(someNumber: 451)

    }
    func generatedNumber(from: Int, to: Int) -> Int {
        let numA = Int.random(in: from...to)
        return numA
    }

    // Задача 0. Вывести на экран наибольшее из двух чисел
    func higestNumber() ->  Int {
        let firstNumb = generatedNumber(from: 50, to: 170)
        let secondNumb = generatedNumber(from: 23, to: 170)
        print("Это первое число: \(firstNumb)")
        print("Это второе число: \(secondNumb)")
        if firstNumb == secondNumb {
            let total = firstNumb
            print("Числа равны \(firstNumb)==\(secondNumb)")
            return total
        }
        if firstNumb < secondNumb {
            let total = secondNumb
            print("Второе число \(secondNumb) больше первого \(firstNumb)")
            return total
        }
        if firstNumb > secondNumb {
            let total = firstNumb
            print("Второе число \(secondNumb) меньше первого \(firstNumb)")
            return total
        }
        else {
            // вы нужденный элс так как xcode ругался на то что он невидет ретерна
            return firstNumb
        }
    }
    // Задача 1. Вывести на экран квадрат и куб числа
    func squarAndCube() -> (Int, Int) {
            let randomNumber = generatedNumber(from: 1, to: 7)
            var squarNumb: Int
            var cubeNumb: Int
            cubeNumb = randomNumber * randomNumber
            squarNumb = randomNumber * randomNumber * randomNumber
            print("Квадрат числа: \(randomNumber) равен: \(cubeNumb)")
            print("Куб числа: \(randomNumber) равен: \(squarNumb)")
            return (cubeNumb, squarNumb)
        }

    // Задача 2. Вывести на экран все числа до заданного и в обратном порядке до 0
    func recount(_ numB: Int) -> [Int] {
        print("Исходное число: \(numB)")
        var someArray: [Int] = []
        var standartOrder = 0
        var reversOrder = numB
        reversOrder += 1
        for _ in 0..<numB  {
            standartOrder += 1
            reversOrder -= 1
            print("Порядок увеличения: \(standartOrder) Обратный порядок: \(reversOrder)")
            someArray.append(reversOrder)
        }
        return someArray
    }
    // Задача 3. Подсчитать общее количество делителей числа и вывести их
    func totalNumberOfDivisors() -> Int {
        let randomNumber = generatedNumber(from: 1, to: 1000)
        var counterNumber = 0
        var displayNumber = 0
        var xNumber = 0
        print("Для этого числа: \(randomNumber) ищем делители")
        for _ in 0..<randomNumber {
            counterNumber  += 1
            displayNumber = randomNumber % counterNumber
            if displayNumber == 0 {
                xNumber += 1
                print("Это делитель: \(counterNumber)")
            }
        }
        print("Кол-во делителей:  \(xNumber)")
        return xNumber
    }
    // Задача 4. Проверить, является ли заданное число совершенным и найти их (делители)
    func perfectNunber() {
        let randomNumber = generatedNumber(from: 1, to: 10000)
        var resultNuber = 0
        var summOfDivisor = 0
        var loopNum = 0
        print("Случайное число от 1 до 10000: \(randomNumber)")
        for _ in 1...randomNumber {
            loopNum += 1
            resultNuber = randomNumber % loopNum
            if resultNuber == 0 {
                summOfDivisor += loopNum
                print("Это делитель \(loopNum)")
                if summOfDivisor != randomNumber {
                    print("Это число не совершенно: \(summOfDivisor)")
                }
                else {
                    print("Это число совершенно: \(summOfDivisor)")
                }
                break
            }
        }
    }
    // Задача 5. Остров Манхэттен был приобретен поселенцами за $24 в 1826 г. Каково было бы в настоящее время состояние их счета, если бы эти 24 доллара были помещены тогда в банк под 6% годового дохода?
    func interestOnDeposit(_ year: Int) -> Double {
        let percentInYear: Double = 6
        let buyYear = 1826
        let ourDays = year
        let startPrice: Double = 24
        var bodyPlusPercent: Double = startPrice / 100 * percentInYear + startPrice
        print("Состояние их счета в \(buyYear) году составляет: \(bodyPlusPercent) $")
        for i in buyYear-2...ourDays {
            bodyPlusPercent = bodyPlusPercent / 100 * 6 + bodyPlusPercent
            print("Состояние их счета в \(i) году составляет: \(bodyPlusPercent) $")
        }
        return bodyPlusPercent
    }
    // Задача 6. Ежемесячная стипендия студента составляет 700 гривен, а расходы на проживание превышают ее и составляют 1000 грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3%. Определить, какую нужно иметь сумму денег, чтобы прожить учебный год (10 месяцев), используя только эти деньги и стипендию.
    func scholarship(scholatshipInMounth: Int, costsNumb: Int, inflationNumb: Int) ->   Int {
        let scholatshipInMounth = scholatshipInMounth
        let costsNumb = costsNumb
        let inflationNumb = inflationNumb
        let academicYear = 10
        var costsInflation = 0
        costsInflation = costsNumb / 100 * inflationNumb + costsNumb
        print("В 1-й месяц нужно к степендии добавить \(costsInflation - scholatshipInMounth) грн")
        var needMoney = 0
        var allNeedMoney = 0
        for i in 2...academicYear {
            costsInflation = costsInflation / 100 * inflationNumb + costsInflation
            needMoney = costsInflation - scholatshipInMounth
            print("В \(i)-й месяц нужно к степендии добавить \(needMoney) грн.")
            allNeedMoney += needMoney
        }
        return allNeedMoney

    }
    // Задача 7. У студента имеются накопления 2400 грн. Ежемесячная стипендия составляет 700 гривен, а расходы на проживание превышают ее и составляют 1000 грн. в месяц. Рост цен ежемесячно увеличивает расходы на 3%. Определить, сколько месяцев сможет прожить студент, используя только накопления и стипендию.
    func zadachyaIII(accumulation: Int, scholarshipInMounht: Int) {
        let accumulation = accumulation
        let scholarshipInMounht = scholarshipInMounht
        let costsNumb = 1000
        let pricesUpIn = 3
        var costsInflation = 0
        var breakPiggiBank = accumulation
        costsInflation = costsNumb / 100 * pricesUpIn + costsNumb
        var needMoney = 0
        for i in 1...10 {
            costsInflation = costsNumb / 100 * pricesUpIn + costsNumb
            needMoney = costsInflation - scholarshipInMounht
            breakPiggiBank -= needMoney
            print("На \(i) месяц останется денег \(breakPiggiBank) в копилке")
            if breakPiggiBank < needMoney {
                print("\n Осталось накоплений: \(breakPiggiBank) + степендия \(scholarshipInMounht)")
                print(" Этого явно мало!!!")
                break
            }
        }
    }
    // Задача 8. 2хзначную цело численную переменную типа 25, 41, 12. После выполнения вашей программы у вас в другой переменной должно лежать это же число только задом на перед 52, 14, 21
    func inversion(someNumber: Int) -> Int {
        let someNumber = someNumber
        var endNumber = 0
        print("Некоторое число       - \(someNumber)")
        if someNumber <= 99 {
            var a = someNumber / 10
            var b = someNumber % 10
            endNumber = b * 10 + a
            print("Инвертированное число - \(endNumber)")
        }
        if 100 <= someNumber && someNumber <= 999 {
            var a = someNumber / 100
            var b = (someNumber - a * 100) / 10
            var c = (someNumber - a * 100) % 10
            endNumber = c * 100 + b * 10 + a
            print("Инвертированное число - \(endNumber)")
        }
        return endNumber
    }

}

